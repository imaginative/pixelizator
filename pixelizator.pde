final String inputFile = "/tmp/inputFileName.jpg";
final int SCALE_X = 900;
final int SCALE_Y = 640;
PImage image;
float _RADIUS;
float radius;
void settings () {
  size(SCALE_X, SCALE_Y);

}

void setup() {
  frameRate(25);
  colorMode(RGB, 255); 
  image = loadImage (inputFile);
  image.resize(SCALE_X, SCALE_Y);
  _RADIUS = 5;
  radius = _RADIUS;
  loop = 0;
}
int loop;
void draw(){
  if (loop%6 == 0) {
    staticMode();
  }
  loop++;
  //dinamicMode();


}

void staticMode() {
  background (255);
  image.loadPixels();
  noStroke();
  for (float i = _RADIUS/2.0; i < height-1; i += _RADIUS) {
    for (float j = _RADIUS/2.0; j < width-1; j += _RADIUS) {
      color c = image.pixels[round(i)*image.width+round(j)];
      float mean = sqrt(blue(c)*blue(c)+green(c)*green(c)+red(c)*red(c));
      radius = _RADIUS*(255-brightness(c))/255.0;
      //radius = _RADIUS*(255*sqrt(3)-mean)/(255*sqrt(3));
      fill (c);
      ellipse (j+random(2),i+random(2),radius+random(2),radius+random(2));
    }
  }
  image.updatePixels();
}

void dinamicMode () {
  background (255);
  
  image.loadPixels();
  radius = 20;//+random(-3,3);

  noStroke();

  for (float i = radius/2.0; i < height-1; i += radius) {
    for (float j = radius/2.0; j < width-1; j += radius) {
      
      
      if (random(1) < 0.02) {
        fill ( image.pixels[round(random(height-1))*SCALE_X+round(random(width-1))] );
      }
      else {
        fill (image.pixels[round(i)*SCALE_X+round(j)]);
      }

      
      if (random(1) < 0.5) {
        rect (j+random(6)-radius/2,i-radius/2+random(6),radius+random(5),radius+random(5));
      }
      else
        ellipse (j+random(3),i+random(6),radius+random(5),radius+random(5));
    }
  }
  
  image.updatePixels();
  //saveFrame("results/gubi-#####.png");
}
